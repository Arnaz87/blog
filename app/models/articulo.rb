class Articulo < ActiveRecord::Base

  has_and_belongs_to_many :categorias

  def texto=(texto)
    article_manager = ArticleManager.new titulo
    article_manager.write texto
    self.url = article_manager.url
  end

  def texto
    return nil if new_record?
    article_manager = ArticleManager.new titulo
    article_manager.read
  end

  def self.paginate(data)
    if dv(data[:q])
      qs = data[:qs]
      qs = 'titulo' if !dv(qs)
      query = where(qs => data[:q])
    else
      query = all
    end
    if dv(data[:o])
      query = query.order(data[:o])
    end
    return query.all
  end

  def self.dv (data)
    return !((data == nil) || (data == '') || (data == ' '))
  end

end
