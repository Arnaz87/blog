class Categoria < ActiveRecord::Base
  self.table_name = 'categorias'

  has_and_belongs_to_many :articulos

end
