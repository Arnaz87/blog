module ArticulosHelper
  def format_html(texto)
    texto.gsub(/\n/, '<br>').html_safe
  end
end
