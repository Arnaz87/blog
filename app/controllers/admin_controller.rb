class AdminController < ApplicationController

  def home
  end

  def sql
    render layout: nil
  end

  def execute_sql
    results = ActiveRecord::Base.connection.exec_query(params[:comando])
    text = "<tr>"
    results.columns.each do |c|
      text += "<th>#{c}</th>"
    end
    text += "</tr>"
    results.rows.each do |r|
      text += "<tr>"
      r.each do |d|
        text += "<td>#{d}</td>"
      end
      text += "</tr>"
    end
    render text: "<table>#{text}</table>"
    return
  rescue Exception => e
    render text: e.message
  end

end