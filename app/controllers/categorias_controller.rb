class CategoriasController < ApplicationController

  def index
    @categorias = Categoria.all
  end

  def new
    @categoria = Categoria.new
  end

  def create
    params.permit!
    @categoria = Categoria.new params[:categoria]
    @categoria.save
    redirect_to categorias_path
  end

  def destroy
    @categoria = Categoria.find params[:id]
    if @categoria.articulos.count == 0
      @categoria.delete
      flash[:notice] = "Categoria eliminada con éxito!"
    else
      flash[:alert] = "No se pueden eliminar categorias con articulos"
    end
    redirect_to categorias_path
  end

  def link_empty
    link_to 'Link Vacio', '#'
  end

end
