class ArticulosController < ApplicationController

  def index
    if params[:categoria_id] == nil
      @categoria = nil
      @articulos = Articulo.paginate params
    else
      @categoria = Categoria.find params[:categoria_id]
      @articulos = @categoria.articulos
    end
  end

  def show
    @articulo = Articulo.find params[:id]
  end

  def new
    @articulo = Articulo.new
    @path = create_articulo_path
  end

  def edit
    @articulo = Articulo.find params[:id]
    @path = update_articulo_path(@articulo)
  end

  def create
    @articulo = Articulo.new params_articulos
    @categorias = params[:articulo][:categorias]
    @categorias = {} if @categorias == nil
    @categorias.each do |key, value|
      categoria = Categoria.find value
      @articulo.categorias << categoria
    end
    @articulo.save
    redirect_to articulos_path
  end

  def update
    @articulo = Articulo.find params[:id]
    @articulo.update params_articulos
    redirect_to articulos_path
  end

  def destroy
    @articulo = Articulo.find params[:id]
    @articulo.delete
    redirect_to articulos_path
  end

  private
    def params_articulos
      params[:articulo].clone.permit!.delete_if{|key, value| key == 'categorias'}
    end 

end
