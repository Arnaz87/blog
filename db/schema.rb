# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140916135003) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articulos", force: true do |t|
    t.string   "titulo"
    t.integer  "usuario_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
  end

  create_table "articulos_categorias", force: true do |t|
    t.integer "articulo_id"
    t.integer "categoria_id"
  end

  add_index "articulos_categorias", ["articulo_id"], name: "index_articulos_categorias_on_articulo_id", using: :btree
  add_index "articulos_categorias", ["categoria_id"], name: "index_articulos_categorias_on_categoria_id", using: :btree

  create_table "categorias", force: true do |t|
    t.string "nombre"
  end

end
