class CreateArticulos < ActiveRecord::Migration
  def change
    create_table :articulos do |t|
      t.string :titulo
      t.text :texto
      t.integer :categoria_id
      t.integer :usuario_id

      t.timestamps
    end
  end
end
