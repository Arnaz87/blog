class CreateArticulosCategorias < ActiveRecord::Migration
  def change
    create_table :articulos_categorias do |t|
      t.references :articulo, index: true
      t.references :categoria, index: true
    end
    remove_column :articulos, :categoria_id
  end
end
