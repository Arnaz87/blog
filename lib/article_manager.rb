class ArticleManager

  def initialize(name)
    @name = name
  end

  def file_name
    "#{@name.downcase.gsub(" ", "_")}.txt"
  end

  def full_path
    "#{Rails.root}/public/articulos/#{file_name}"
  end

  def url
    "/articulos/#{file_name}"
  end

  def exists?
    File::exists?(full_path)
  end

  def read
    return nil if !exists?
    texto = ""
    File.open(full_path, "r") do |f|
      f.each_line do |line|
        texto << line
      end
    end
    texto
  end

  def write(texto)
    bytes = 0
    File.open(full_path, "w") do |f|
      bytes = f.write texto
    end
    bytes
  end

  def delete
    return false if !exists?
    File.delete(full_path)
    return true
  rescue
      return false
  end
end