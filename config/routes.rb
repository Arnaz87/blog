Rails.application.routes.draw do

  root 'admin#home'

  get 'articulos', to: 'articulos#index', as: 'articulos'
  get 'articulos/new', to: 'articulos#new', as: 'new_articulos'
  get 'articulo/:id', to: 'articulos#show', as: 'articulo'
  get 'articulo/:id/edit', to: 'articulos#edit', as: 'edit_articulo'
  post 'articulos', to: 'articulos#create', as: 'create_articulo'
  patch 'articulos/:id', to: 'articulos#update', as: 'update_articulo'
  delete 'articulo/:id', to: 'articulos#destroy', as: 'delete_articulo'

  get 'categorias', to: 'categorias#index', as: 'categorias'
  get 'categorias/new', to:'categorias#new', as: 'new_categoria'
  post 'categorias', to: 'categorias#create', as: 'create_categoria'
  delete 'categorias/:id', to: 'categorias#destroy', as: 'delete_categoria'

  get 'categorias/:categoria_id/articulos', to: 'articulos#index', as: 'categoria_articulos'
  get 'categorias/:categoria_id/articulos/new', to: 'articulos#new', as: 'new_categoria_articulos'

  get 'admin/sql', to: 'admin#sql', as: 'sql'
  post 'admin/sql', to: 'admin#execute_sql', as: 'execute_sql'

  # Comentarios de ayuda:
    # The priority is based upon order of creation: first created -> highest priority.
    # See how all your routes lay out with "rake routes".

    # You can have the root of your site routed with "root"
    # root 'welcome#index'

    # Example of regular route:
    #   get 'products/:id' => 'catalog#view'

    # Example of named route that can be invoked with purchase_url(id: product.id)
    #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

    # Example resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products

    # Example resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection do
    #       get 'sold'
    #     end
    #   end

    # Example resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end

    # Example resource route with more complex sub-resources:
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', on: :collection
    #     end
    #   end

    # Example resource route with concerns:
    #   concern :toggleable do
    #     post 'toggle'
    #   end
    #   resources :posts, concerns: :toggleable
    #   resources :photos, concerns: :toggleable

    # Example resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end
  #Final de los comentarios
end
