require 'rails_helper'

RSpec.describe Articulo, :type => :model do
  it "debe setear url con la direccion del archivo creado" do
    categoria = Categoria.create(nombre: "Categoria 1")
    attributes = {titulo: "new article", texto: "bla, bla", categoria: categoria }
    articulo = Articulo.new(attributes)
    expect(articulo.save).to be true
    expect(articulo.url).to eq("/articulos/new_article.txt")
  end
end
