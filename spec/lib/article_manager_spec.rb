require 'rails_helper'

RSpec.describe ArticleManager, :type => :lib do
  def copy_fixture
    FileUtils.cp("#{Rails.root}/spec/fixtures/new_article.txt", "#{Rails.root}/public/articulos/")
  end

  def delete_fixture
    FileUtils.rm("#{Rails.root}/public/articulos/new_article.txt", force: true)
  end

  let(:article_manager) { ArticleManager.new("new article") }
  
  describe "#file_name" do
    it "devuelte el nombre del articulo convertido en un nombre de archivo valido y extension txt" do
      expect(article_manager.file_name).to eq("new_article.txt")
    end
  end

  describe "#full_path" do
    it "devuelte la direccion completa del articulo" do
      expect(article_manager.full_path).to eq("#{Rails.root}/public/articulos/new_article.txt")
    end
  end

  describe "#exists?" do
    context "cuando el archivo existe" do
      it "devuelve true" do
        copy_fixture
        expect(article_manager.exists?).to be true
        delete_fixture
      end
    end

    context "cuando el archivo no existe" do
      it "devuelve false" do
        delete_fixture
        expect(article_manager.exists?).to be false
      end
    end
  end

  describe "#read" do
    context "si archivo existe" do
      it "devuelve el contenido del articulo" do
        copy_fixture
        expect(article_manager.read).to eq("Esto es un articulo de prueba !!!")
        delete_fixture
      end
    end
    context "si archivo no existe" do
      it "devuelve nil" do
        expect(article_manager.read).to be nil
      end
    end
  end

  describe "#write" do
    context "si archivo existe" do
      it "sobre escribe el texto del archivo con el nuevo texto" do
        copy_fixture
        expect(article_manager.read).to eq("Esto es un articulo de prueba !!!")
        expect(article_manager.write("Prueba")).to eq(6)
        expect(article_manager.read).to eq("Prueba")
        delete_fixture
      end
    end
    context "si archivo no existe" do
      it "crea un nuevo archivo y con el texto nuevo" do
        expect(article_manager.exists?).to be false
        expect(article_manager.write("Prueba")).to eq(6)
        expect(article_manager.exists?).to be true
        delete_fixture
      end
    end  
  end

  describe "#write" do
    context "si archivo existe" do
      it "sobre escribe el texto del archivo con el nuevo texto" do
        copy_fixture
        expect(article_manager.read).to eq("Esto es un articulo de prueba !!!")
        expect(article_manager.write("Prueba")).to eq(6)
        expect(article_manager.read).to eq("Prueba")
        delete_fixture
      end
    end
    context "si archivo no existe" do
      it "crea un nuevo archivo y con el texto nuevo" do
        expect(article_manager.exists?).to be false
        expect(article_manager.write("Prueba")).to eq(6)
        expect(article_manager.exists?).to be true
        delete_fixture
      end
    end  
  end

  describe "#delete" do
     context "si archivo existe" do
      it "elimina el archivo y devulve true" do
        copy_fixture
        expect(article_manager.exists?).to be true
        expect(article_manager.delete).to be true
        expect(article_manager.exists?).to be false
      end
    end
    context "si archivo no existe" do
      it "crea un nuevo archivo y con el texto nuevo" do
        expect(article_manager.delete).to be false
      end
    end   
  end

end
